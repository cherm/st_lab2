module.exports = {
    isUserNameValid: function (username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        // Kob-> kob
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },

    isAgeValid: function (age) {
        //must be number
        if (isNaN(age)) {
            return false;
        }
        // age 18-100
        if (age < 18 || age > 100) {
            return false;
        }
        return true;
    },

    isPasswordValid: function (password) {
        if (password.length < 8) {
            return false;
        }
        const PWUpper = /[A-Z]/
        if (!PWUpper.test(password)) {
            return false;
        }
        const PWnum = /(?=.*\d{3})/
        if (!PWnum.test(password)) {
            return false;
        }
        const PWchrt = /(?=.*\W)/
        if (!PWchrt.test(password)) {
            return false;
        }
        return true;
    },

    isDateValid: function (day, month, year) {
        if (month == 1|| month == 3|| month == 5|| month == 7|| month == 8|| month == 10|| month == 12) {
            if (day < 1 || day > 31) {
                return false;
            } 
        } 
        if(month == 2) {
            if (year%400 !== 0){
                if(day<1 || day > 28){
                    return false;
                }
            }
        }else{
            if (day < 1 || day > 30) {
                return false;
            } 
        }
        if (month < 1 || month > 12) {
            return false;
        }
        if (year < 1970 || year > 2020) {
            return false;
        }
        return true;
    }


}
